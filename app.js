const createError = require( 'http-errors' );
const express = require( 'express' );
const path = require( 'path' );
const favicon = require( 'serve-favicon' );
const cookieParser = require( 'cookie-parser' );
const logger = require( 'morgan' );
const fs = require( 'fs' );
const db = require( './db' );

const indexRouter = require( './routes/index' );
const usersRouter = require( './routes/profile' );
const blogRouter = require( './routes/blog' );
const signupRouter = require( './routes/signup' );
const succesRouter = require( './routes/succes' );
const singleRouter = require( './routes/single' );

const app = express();

// Set up db
db.init( ( err ) => {
  if ( err ) {
throw err;
}
  console.log( 'Connected to DB!' );
} );

// view engine setup
app.set( 'views', path.join( __dirname, 'views' ) );
app.set( 'view engine', 'pug' );

app.use( logger( 'dev' ) );
app.use( express.json() );
app.use( express.urlencoded( { extended: false } ) );
app.use( cookieParser() );
app.use( express.static( path.join( __dirname, '/public' ) ) );
app.use( '/css', express.static( __dirname + '/node_modules/bootstrap/dist/css' ) );
app.use( '/js', express.static( __dirname + '/node_modules/bootstrap/dist/js' ) );
app.use( '/assets', [
  express.static( __dirname + '/node_modules/jquery/dist/' ),
  express.static( __dirname + '/node_modules/materialize-css/dist/' )
] );

app.use( '/', indexRouter );
app.use( '/profile', usersRouter );
app.use( '/blog', blogRouter );
app.use( '/signup', signupRouter );
app.use( '/succes', succesRouter );
app.use( '/single', singleRouter );


if ( 'staging' === app.get( 'env' ) || 'production' === app.get( 'env' ) ) {

	// If staging or production, compile the SASS files
	require( './lib/production/prepareProduction' )();
} else {

	// If not staging or production, just compile the libs.scss
	require( './lib/compileSass' ).compileSassLibs().catch( console.error );

	// Also include the CSS routes for on-the-fly compiling
	const css = require( './routes/css' );
	app.use( '/css', css );
}

// catch 404 and forward to error handler
app.use( function( req, res, next ) {
  next( createError( 404 ) );
} );

// error handler
app.use( function( err, req, res, next ) {

  // set locals, only providing error in development
  res.locals.message = err.message;
  res.locals.error = 'development' === req.app.get( 'env' ) ? err : {};

  // render the error page
  res.status( err.status || 500 );
  res.render( 'error' );
} );

if ( 'production' === app.get( 'env' ) ) {

  var accessLogStream = fs.createWriteStream( __dirname + '/logs/' + 'access.log', {flags: 'a'} );
  app.use( logger( {stream: accessLogStream} ) );
} else {
  app.use( logger( 'dev' ) ); //log to console on development
}

module.exports = app;
