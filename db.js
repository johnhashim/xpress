const MongoClient = require( 'mongodb' ).MongoClient;
const dbname = 'xpress2';
let _db;
let db = module.exports = {};
db.init = ( callback ) => {
    MongoClient.connect( 'mongodb://localhost:27017/', ( err, client ) => {
        if ( err ) {
            throw err;
        }
        _db = client.db( dbname );
        return callback( err );


    } );
};
db.close = () => {
    console.log( 'closing DB connection' );
    return _db.close();
};

db.insertDetail = ( db, callback ) => {
    const collection = _db.collection( 'details' );
    collection.insertOne( db, function( err, result ) {
        console.log( 'Inserted document into the collection' );
        callback( result );
    } );
};

db.createUser = ( db, callback ) => {
    console.log( db.email );
    const collection = _db.collection( 'details' );
    collection.find( {
        email: db.email
    } ).toArray( ( err, users ) => {
        if ( 1 === users.length ) {
            callback( 'Error: User already exists' );
        } else {
    collection.insertOne( db, function( err, result ) {
                callback( result );
            } );
        }
    } );
};
db.getPost = ( postId, callback ) => {
    const collection = _db.collection( 'posts' );
    collection.findOne( {
        userId: postId
    }, ( err, postData ) => {
        return callback( postData );
    } );
};
