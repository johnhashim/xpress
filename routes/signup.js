const express = require( 'express' );
const people = require( '../data.json' );
const router = express.Router();
const app = express();
const crypto = require( 'crypto' );
const bodyParser = require( 'body-parser' );

const db = require( '../db' );
app.use( bodyParser.json() );
app.use( bodyParser.urlencoded( { // to support URL-encoded bodies
	extended: true
} ) );

var getHash = ( pass, phone ) => {

				var hmac = crypto.createHmac( 'sha512', phone );

				//passing the data to be hashed
				data = hmac.update( pass );

				//Creating the hmac in the required format
				gen_hmac = data.digest( 'hex' );

				//Printing the output on the console
				console.log( 'hmac : ' + gen_hmac );
				return gen_hmac;
};

// Sign-up function starts here. . .
router.post( '/', function( req, res ) {
  console.log( 'here' );

	var name = req.body.name;
	var email = req.body.email;
	var pass = req.body.password;
	var phone = req.body.phone;
	var password = getHash( pass, phone );


	const data = {
		'name': name,
		'email': email,
		'password': password,
		'phone': phone
  };

  db.createUser( data,  ( result, err ) => {
    console.log( result );
  } );


	//console.log( 'DATA is ' + JSON.stringify( data ) );
	res.set( {
		'Access-Control-Allow-Origin': '*'
	} );
  return res.redirect( '/succes' );

} );

/* GET home page. */
router.get( '/', function( req, res, next ) {
  res.render( 'signup', { title: 'Homepage',
  people: people.profiles } );
} );

module.exports = router;
