const express = require( 'express' );
const people = require( '../data.json' );
const router = express.Router();

/* GET profile page. */
router.get( '/', function( req, res, next ) {
  const person = people.profiles.find( p => p.id === req.query.id );
  res.render( 'profile', {title: `About ${person.firstname} ${person.lastname}`,
  person } );
} );

module.exports = router;
