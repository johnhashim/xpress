const express = require( 'express' );
const router = express.Router();
const db = require( '../db' );

/* GET users listing. */
router.get( '/', function( req, res, next ) {
    const id = req.query.id;
    db.getPost( id, ( postData, err ) => {
        const dataToSend = {
            title: 'About ' + postData.firstname + ' ' + postData.lastname,
            person: postData
        };
        return res.render( 'single', dataToSend );
    } );

} );

module.exports = router;
