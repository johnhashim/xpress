const express = require( 'express' );
const people = require( '../data.json' );
const router = express.Router();

/* GET profile page. */
router.get( '/', function( req, res, next ) {
  const person = people.portifolios.find( p => p.id === req.query.id );
  res.render( 'portifolio', {title: `About ${person.name}`,
  person } );
} );

module.exports = router;
