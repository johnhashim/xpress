const express = require( 'express' );
const people = require( '../data.json' );
const router = express.Router();

/* GET home page. */
router.get( '/', function( req, res, next ) {
  res.render( 'succes', { title: 'Homepage',
  people: people.profiles } );
} );

module.exports = router;
